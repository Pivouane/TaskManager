const mongoose = require('mongoose');
const Task = require('./models/Task'); // Assurez-vous que ce chemin est correct

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/taskmanager';

mongoose.connect(mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', async () => {
  console.log('Connected to MongoDB');

  const count = await Task.countDocuments();
  if (count === 0) {
    console.log('No tasks found, seeding database...');
    
    const initialTasks = [
      { title: 'Complete online JavaScript course', completed: true, createdAt: new Date() - 86400000, completedAt: new Date() },
      { title : 'Jog around the park 3x', completed: true, createdAt: new Date() - 3600000 * 3, completedAt: new Date() },
      { title: '10 minutes meditation', completed: false, createdAt: new Date() - 3600000 * 5 },
    ];
    
    await Task.insertMany(initialTasks);
    console.log('Database seeded with initial tasks.');
  } else {
    console.log('Tasks already exist, skipping seeding.');
  }

  mongoose.connection.close();
});
