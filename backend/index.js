require('./sentry')
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const Task = require('./models/Task');
const promClient = require('prom-client');
const Sentry = require("@sentry/node");

const app = express();

app.use(cors({
  origin: 'http://localhost:3000',
}));

app.use(express.json());

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/taskmanager';
mongoose.connect(mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});


const collectDefaultMetrics = promClient.collectDefaultMetrics;
collectDefaultMetrics({ timeout: 5000 });

const taskCounter = new promClient.Counter({
  name: 'task_counter',
  help: 'Number of tasks created',
  
});

const completedTaskGauge = new promClient.Gauge({
  name: 'completed_task_gauge',
  help: 'Number of completed tasks',
});

const completionTimeHistogram = new promClient.Histogram({
  name: 'completion_time_histogram',
  help: 'Time taken to complete a task',
  buckets: [0.1, 1, 2, 3, 4, 5, 10, 15, 20],
});

const wordListMetric = new promClient.Gauge({
  name: 'word_list_metric',
  help: 'Word list metric',
  labelNames: ['word'],
});

const responseTimeHistogram = new promClient.Histogram({
  name: 'response_time_histogram',
  help: 'Time taken to respond to a request',
  labelNames: ['route'],
  buckets: [0.1, 5, 15, 30, 60],
});


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', async () => {
  console.log('Connected to MongoDB');
  taskCounter.reset();
  completedTaskGauge.reset();
  completionTimeHistogram.reset();

  const persistedTasks = await Task.find();
  taskCounter.inc(persistedTasks.length);

  persistedTasks.forEach(task => {
    if (task.completed) {
      completedTaskGauge.inc();
    }

    if (task.completedAt) {
      const completionTime = (task.completedAt - task.createdAt) / 1000 / 60 / 60 / 24;
      completionTimeHistogram.observe(completionTime);
    }

    const words = task.title.split(' ');
    words.forEach(word => {
      wordListMetric.inc({ word });
    });
  });
  console.log('Tasks loaded');
});

app.use((req, res, next) => {
  const end = responseTimeHistogram.startTimer();
  res.on('finish', () => {
    responseTimeHistogram.observe({ route: req.route.path }, end());
    end({ route: req.route.path });
  });
  next();
});

app.get('/metrics', async (req, res) => {
  res.set('Content-Type', promClient.register.contentType);
  res.end(await promClient.register.metrics());
});

app.get('/tasks', async (req, res) => {
  try {
    const tasks = await Task.find();
    res.json(tasks);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.post('/tasks', async (req, res) => {
  try {
    const task = new Task(req.body);
    if (!task.title) {
      res.status(400).json({ error: 'Title is required' });
      return;
    }
    if (typeof task.completed !== 'boolean') {
      res.status(400).json({ error: 'Completed must be a boolean' });
      return;
    }
    const savedTask = await task.save();
    taskCounter.inc();
    const words = task.title.split(' ');
    words.forEach(word => {
      wordListMetric.inc({ word });
    });
    res.status(201).json(task);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/word-list', async (req, res) => {
  try {
    const words = await Task.find().distinct('title');
    res.json(words);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/hard-word-list', async (req, res) => {
  try {
    const words = ['hard', 'words', 'list'];
    res.json(words);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.put('/tasks/:id/completed', async (req, res) => {
  try {
    const updatedTask = await Task.findByIdAndUpdate(
      req.params.id,
      { completed: req.body.completed },
      { new: true }
    );
    if (updatedTask.completed) {
      completedTaskGauge.inc();
    }
    if (updatedTask.completedAt) {
      const completionTime = updatedTask.completedAt - updatedTask.createdAt / 1000 / 60 / 60 / 24;
      completionTimeHistogram.observe(completionTime);
    }
    res.json(updatedTask);
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
});
const port = process.env.PORT || 3001;
Sentry.setupExpressErrorHandler(app);
app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
